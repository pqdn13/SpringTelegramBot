package edu.telegram.dao;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name = "item")
@SqlResultSetMapping(
     name = "itemResult",
     entities = @EntityResult(entityClass = Item.class)
)
public class Item implements Serializable {
    private Long id;
    private int version;
    private String name;
    private String description;
    private Date addDate;
    private Set<ItemDetail> itemDetails = new HashSet<ItemDetail>();

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Version
    @Column(name = "VERSION")
    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Column(name = "NAME")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "ADD_DATE")
    public Date getBirthDate() {
        return this.addDate;
    }

    public void setBirthDate(Date addDate) {
        this.addDate = addDate;
    }

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL,
        orphanRemoval = true)
    public Set<ItemDetail> getItemDetails() {
        return this.itemDetails;
    }

    public void setItemDetails(Set<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
    }

    public void addItemDetail(ItemDetail itemDetail) {
        itemDetail.setItem(this);
        getItemDetails().add(itemDetail);
    }

    public void removeItemDetail(ItemDetail itemDetail) {
        getItemDetails().remove(itemDetail);
    }

    @Override
    public String toString() {
        return "Item - Id: " + id + ", Name: " + name
            + ", Description: " + description + ", AddDate: " + addDate;
    }
}
