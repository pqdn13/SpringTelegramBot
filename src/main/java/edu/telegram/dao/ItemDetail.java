package edu.telegram.dao;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "item_detail")
public class ItemDetail implements Serializable {
    private Long id;
    private int version;
    private String price;
    private String url;
    private Item item;

    public ItemDetail() {
    }

    public ItemDetail(String price, String url) {
        this.price = price;
        this.url = url;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return this.id; 
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Version
    @Column(name = "VERSION")
    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Column(name = "PRICE")
    public String getPrice() {
        return this.price;
    } 

    public void setPrice(String price) {
        this.price = price;
    }

    @Column(name = "URL")
    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    public Item getItem() {
        return this.item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "Item Detail - Id: " + id + ", Item id: "
            + getItem().getId() + ", Price: "
            + price + ", Url: " + url;
    }
}
