package edu.telegram.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ItemRepository extends CrudRepository<Item, Long> {
    String FIND_WITH_QUERY = "select distinct c  from Item c left join fetch c.itemDetails t where c.id = :id";

    List<Item> findById(Long id);

    List<Item> findItemsByIdBetween(Long from, Long to);

    @Query(FIND_WITH_QUERY)
    List<Item> findByIdWithDetails(@Param("id") Long id);
}
