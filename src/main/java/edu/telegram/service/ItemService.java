package edu.telegram.service;

import edu.telegram.dao.Item;

import java.util.List;

public interface ItemService {
    List<Item> findAll();

    List<Item> findById(Long id);

    List<Item> findItemsByIdBetween(Long from, Long to);

    List<Item> findByIdWithDetails(Long id);
}
