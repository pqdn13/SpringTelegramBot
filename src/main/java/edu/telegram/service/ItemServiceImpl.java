package edu.telegram.service;

import com.google.common.collect.Lists;

import edu.telegram.dao.Item;
import edu.telegram.dao.ItemRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("springJpaItemService")
@Repository
@Transactional
public class ItemServiceImpl implements ItemService {
    @Autowired
    private ItemRepository itemRepository;

    @Transactional(readOnly = true)
    public List<Item> findAll() {
        return Lists.newArrayList(itemRepository.findAll());
    }

    @Transactional(readOnly = true)
    public List<Item> findById(Long id) {
        return itemRepository.findById(id);
    }

    @Override
    public List<Item> findItemsByIdBetween(Long from, Long to) {
        return itemRepository.findItemsByIdBetween(from,to);
    }

    @Transactional(readOnly = true)
    public List<Item> findByIdWithDetails(Long id) {
        return Lists.newArrayList(itemRepository.findByIdWithDetails(id));
    }
}
