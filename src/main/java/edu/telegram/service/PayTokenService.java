package edu.telegram.service;

public interface PayTokenService {
    String getPayToken();
}
