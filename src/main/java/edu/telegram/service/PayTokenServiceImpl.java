package edu.telegram.service;

import edu.telegram.springbot.BotConfig;
import org.springframework.stereotype.Service;

@Service
public class PayTokenServiceImpl implements PayTokenService {
    private BotConfig botConfig = BotConfig.getInstance();

    @Override
    public String getPayToken() {
        return botConfig.getPayTokenBot();
    }
}
