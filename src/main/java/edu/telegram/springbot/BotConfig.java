package edu.telegram.springbot;

import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class BotConfig {
    private static final Logger LOGGER = Logger.getLogger(BotConfig.class);

    private static final String pathProp = "/bot-settings/bot-setting.properties";
    private static final String pathPropTest = "/bot-settings/bot-setting.properties.test";

    private String tokenBot;
    private String nicknameBot;
    private String payTokenBot;

    public static BotConfig getInstance() {
        return Holder.INST;
    }

    public String getTokenBot() {
        return tokenBot;
    }

    public String getNicknameBot() {
        return nicknameBot;
    }

    public String getPayTokenBot() {
        return payTokenBot;
    }

    private BotConfig() {
        String path = isTestMode() ? pathPropTest : pathProp;

        try (InputStream inputStream = getClass().getResourceAsStream(path)) {
            Properties properties = new Properties();
            properties.load(inputStream);
            tokenBot = properties.getProperty("tokenBot");
            nicknameBot = properties.getProperty("nicknameBot");
            payTokenBot = properties.getProperty("payTokenBot");

        } catch (FileNotFoundException e) {
            LOGGER.error("Не найден файл конфигурации бота", e);
        } catch (IOException e) {
            LOGGER.error("Что-то не так с файлом конфигурации", e);
        }
    }

    private boolean isTestMode() {
        return true;
    }

    private static class Holder {
        final static BotConfig INST = new BotConfig();
    }
}
