package edu.telegram.springbot;

import edu.telegram.springbot.container.SelectHandle;
import org.apache.log4j.Logger;
import org.telegram.telegrambots.api.methods.BotApiMethod;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

import java.util.List;

public class SimpleBot extends TelegramLongPollingBot {
    private static final Logger LOGGER = Logger.getLogger(SimpleBot.class);
    private BotConfig botConfig = BotConfig.getInstance();

    @Override
    public void onUpdateReceived(Update update) {
        List<BotApiMethod> apiMethods = SelectHandle.getHandle(update).process(update);
        if (apiMethods == null) {
            return;
        }

        for (BotApiMethod method : apiMethods) {
            try {
                LOGGER.trace(execute(method).toString());
            } catch (TelegramApiRequestException e) {
                LOGGER.error("Telegram api server error", e);
            } catch (TelegramApiException e) {
                LOGGER.error("Telegram api error", e);
            }
        }
    }

    @Override
    public String getBotToken() {
        return botConfig.getTokenBot();
    }

    @Override
    public String getBotUsername() {
        return botConfig.getNicknameBot();
    }
}