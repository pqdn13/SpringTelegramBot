package edu.telegram.springbot.container;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class BotApiMethodContainer {
    private static final Logger LOGGER = Logger.getLogger(BotApiMethodContainer.class);

    private Map<String, BotApiMethodController> controllerMap;

    public static BotApiMethodContainer getInstanse() {
        return Holder.INST;
    }


    public void addBotController(String path, BotApiMethodController controller) {
        if (controllerMap.containsKey(path)) {
            throw new BotApiMethodContainerException("path " + path + " already add");
        }
        LOGGER.trace("add telegram bot controller for path: " +  path);
        controllerMap.put(path, controller);
    }

    public int size() {
        return controllerMap.size();
    }

    public BotApiMethodController getBotApiMethodController(String path) {
        return controllerMap.get(path);
    }

    private BotApiMethodContainer() {
        controllerMap = new HashMap<>();
    }

    private static class Holder {
        final static BotApiMethodContainer INST = new BotApiMethodContainer();
    }

    public Map<String, BotApiMethodController> getControllerMap() {
        return controllerMap;
    }
}
