package edu.telegram.springbot.container;

public class BotApiMethodContainerException extends RuntimeException {
    public BotApiMethodContainerException() {
    }

    public BotApiMethodContainerException(String message) {
        super(message);
    }
}
