package edu.telegram.springbot.container;

import org.telegram.telegrambots.api.methods.BotApiMethod;
import org.telegram.telegrambots.api.objects.Update;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class FakeBotApiMethodController extends BotApiMethodController {
    public FakeBotApiMethodController() {
        super(null, null);
    }

    public FakeBotApiMethodController(Object bean, Method method) {
        super(bean, method);
    }

    @Override
    public boolean successUpdatePredicate(Update update) {
        return false;
    }

    @Override
    public List<BotApiMethod> process(Update update) {
        return new ArrayList<>(0);
    }
}
