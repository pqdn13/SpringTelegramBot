package edu.telegram.springbot.container;

import org.telegram.telegrambots.api.objects.Update;

public class SelectHandle {
    private static BotApiMethodContainer container = BotApiMethodContainer.getInstanse();

    public static BotApiMethodController getHandle(Update update) {
        String path;
        BotApiMethodController controller = null;

        if (update.hasMessage() && update.getMessage().hasText()) {
            path = update.getMessage().getText().split(" ")[0].trim();
            controller = container.getControllerMap().get(path);
            if (controller == null) {
                controller = container.getControllerMap().get("");
            }

        } else if (update.hasCallbackQuery()) {
            path = update.getCallbackQuery().getData().split("/")[1].trim();
            controller = container.getControllerMap().get(path);
            if (controller == null) {
                controller = container.getControllerMap().get("");
            }
        }

        return controller != null ? controller : new FakeBotApiMethodController();
    }
}
