package edu.telegram.springbot.container.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * BotRequestMapping mark method as handler
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface BotRequestMapping {
    /**
     * @return path
     */
    String[] value() default {};

    /**
     * @return types handler
     */
    BotRequestMethod[] method() default {BotRequestMethod.MSG};
}
