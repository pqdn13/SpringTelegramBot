package edu.telegram.springbot.controllers;

import edu.telegram.service.PayTokenService;
import edu.telegram.springbot.container.annotation.BotController;
import edu.telegram.springbot.container.annotation.BotRequestMapping;
import edu.telegram.springbot.container.annotation.BotRequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.api.methods.BotApiMethod;
import org.telegram.telegrambots.api.methods.send.SendInvoice;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.payments.LabeledPrice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@BotController
public class InvoiceSimpleController {
    @Autowired
    private PayTokenService payTokenService;

    @BotRequestMapping(value = "/myPayToken", method = BotRequestMethod.MSG)
    public SendMessage payRequest(Update update) {
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText(payTokenService.getPayToken());
    }


    @BotRequestMapping(value = "/buyItem", method = BotRequestMethod.EDIT)
    public List<BotApiMethod> bayItem(Update update) {

        List<LabeledPrice> price = new ArrayList<>();
        price.add(new LabeledPrice("33", 11100));

        return Arrays.asList(new EditMessageText()
                .setChatId(update.getMessage().getChatId())
                .setMessageId(update.getMessage().getMessageId())
                .setText("Подтвердите ваш выбор, в форме ниже"),

                new SendInvoice()
                        .setChatId(Integer.parseInt(update.getMessage().getChatId().toString()))
                        .setDescription("The best product")
                        .setTitle("The best my item")
                        .setPayload("test")
                        .setProviderToken(payTokenService.getPayToken()) // <--
                        .setStartParameter("StartParam")
                        .setCurrency("RUB")
                        .setPrices(price)
                        .setNeedEmail(true)
        );
    }
}
