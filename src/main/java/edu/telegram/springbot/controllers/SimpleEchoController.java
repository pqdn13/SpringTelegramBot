package edu.telegram.springbot.controllers;

import edu.telegram.springbot.container.annotation.BotController;
import edu.telegram.springbot.container.annotation.BotRequestMapping;
import edu.telegram.springbot.container.annotation.BotRequestMethod;
import org.apache.log4j.Logger;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

@BotController
public class SimpleEchoController {
    static final Logger LOGGER = Logger.getLogger(SimpleEchoController.class);

    @BotRequestMapping(value = "/ok", method = BotRequestMethod.MSG)
    public SendMessage ok(Update update) {
        return new SendMessage()
                .setChatId(update.getMessage().getChatId())
                .setText("okay bro, okay!");
    }
}
