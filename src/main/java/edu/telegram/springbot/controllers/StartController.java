package edu.telegram.springbot.controllers;

import edu.telegram.dao.Item;
import edu.telegram.springbot.container.annotation.BotController;
import edu.telegram.springbot.container.annotation.BotRequestMapping;
import org.apache.log4j.Logger;
import edu.telegram.springbot.container.annotation.BotRequestMethod;
import edu.telegram.utils.Filter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

@BotController
public class StartController {
    static final Logger LOGGER = Logger.getLogger(StartController.class);

    @Autowired
    private Filter shopMenu;

    @BotRequestMapping("/buy")
    public SendMessage generateInitMenu(Update update) {
        SendMessage message = null;
        if (update.hasMessage() && update.getMessage().hasText()) {
            message = new SendMessage()
                    .setChatId(update.getMessage().getChatId().toString())
                    .setText("Хочу это!")
                    .setReplyMarkup(shopMenu.getSubMenu(0L, 4L, 1L));
        }
        return message;
    }

    @BotRequestMapping(value = "ShopMenu", method = BotRequestMethod.EDIT)
    public EditMessageText showShopMenu(Update update) {
        String callData = update.getCallbackQuery().getData();
        int messageId = update.getCallbackQuery().getMessage().getMessageId();
        long chatId = update.getCallbackQuery().getMessage().getChatId();
        Long pageNumber = shopMenu.getPageNumberFromCallback(callData);
        Long from = (pageNumber - 1) * shopMenu.ELEMENTS_PER_PAGE;
        EditMessageText editMessageText = new EditMessageText()
                .setChatId(chatId)
                .setMessageId(messageId)
                .setText("Хочу это!")
                .setReplyMarkup(shopMenu.getSubMenu(from, from + 4, pageNumber));

        return editMessageText;
    }

    @BotRequestMapping(value = "ItemMenu", method = BotRequestMethod.EDIT)
    public EditMessageText showItemMenu(Update update) {
        String callData = update.getCallbackQuery().getData();
        Long pageNumber = shopMenu.getPageNumberFromCallback(callData);
        Item item = shopMenu.getItemFromCallback(callData);
        int messageId = update.getCallbackQuery().getMessage().getMessageId();
        long chatId = update.getCallbackQuery().getMessage().getChatId();

        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();

        List<InlineKeyboardButton> row = new ArrayList<>();

        JSONObject buyButton = new JSONObject();
        JSONObject backButton = new JSONObject();
        buyButton.put("itemId", item.getId());
        backButton.put("pageNumber", item.getId());
        backButton.put("pageNumber", String.valueOf(pageNumber));
        row.add(new InlineKeyboardButton()
                .setText("Купить")
                .setCallbackData("/ItemMenu/?" + buyButton)
        );
        row.add(new InlineKeyboardButton()
                .setText("Назад")
                .setCallbackData("/ShopMenu/?" + backButton)
        );
        keyboard.add(row);

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup().setKeyboard(keyboard);

        EditMessageText editMessageText = new EditMessageText()
                .setChatId(chatId)
                .setText(
                        "[" + item.getName() + "](" + item.getDescription() + "\n"
                )
                .setMessageId(messageId)
                .setReplyMarkup(inlineKeyboardMarkup)
                .enableMarkdown(true);

        return editMessageText;
    }
}
