package edu.telegram.utils;

import edu.telegram.dao.Item;
import edu.telegram.service.ItemService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.panasyuk on 03.08.2017.
 */
@Component
public class Filter {

    @Autowired
    private ItemService itemService;

    private static int pageNumber = 1;

    public static final int ELEMENTS_PER_PAGE = 4;

    public static int getPageNumber() {
        return pageNumber;
    }

    public static void setPageNumber(int pageNumber) {
        Filter.pageNumber = pageNumber;
    }


    public InlineKeyboardMarkup getSubMenu(Long fromIndex, Long toIndex, Long pageNumber) {

        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        Long indexTo = (pageNumber + 1) * ELEMENTS_PER_PAGE;
        JSONObject next = new JSONObject();
        JSONObject prev = new JSONObject();
        next.put("pageNumber", String.valueOf(pageNumber + 1));
        prev.put("pageNumber", String.valueOf(pageNumber - 1));
        if (pageNumber - 1 > 0 && indexTo <= itemService.findAll().size()) {
            row = fillMenuWithItems(fromIndex, toIndex, pageNumber, keyboard);
            row.add(new InlineKeyboardButton().setText("Назад").setCallbackData("/ShopMenu/?" + prev));
            row.add(new InlineKeyboardButton().setText("Вперед").setCallbackData("/ShopMenu/?" + next));
        } else if (pageNumber - 1 <= 0 && indexTo <= itemService.findAll().size()) {
            row = fillMenuWithItems(fromIndex, toIndex, pageNumber, keyboard);
            row.add(new InlineKeyboardButton().setText("Вперед").setCallbackData("/ShopMenu/?" + next));
        } else if (indexTo > itemService.findAll().size()) {
            row = fillMenuWithItems(fromIndex, toIndex, pageNumber, keyboard);
            row.add(new InlineKeyboardButton().setText("Назад").setCallbackData("/ShopMenu/?" + prev));
        }
        keyboard.add(row);
        return new InlineKeyboardMarkup().setKeyboard(keyboard);
    }

    private List<InlineKeyboardButton> fillMenuWithItems(Long fromIndex,
                                                         Long toIndex,
                                                         Long pageNumber,
                                                         List<List<InlineKeyboardButton>> keyboard) {
        List<Item> items = itemService.findItemsByIdBetween(fromIndex, toIndex);
        List<InlineKeyboardButton> row = new ArrayList<>();
        insertItem(row, items.get(0), pageNumber);
        insertItem(row, items.get(1), pageNumber);
        keyboard.add(row);

        row = new ArrayList<>();
        insertItem(row, items.get(2), pageNumber);
        insertItem(row, items.get(3), pageNumber);
        keyboard.add(row);

        row = new ArrayList<>();
        return row;
    }

    private static void insertItem(List<InlineKeyboardButton> row, Item item, long pageNumber) {
        JSONObject object = new JSONObject();
        object.put("itemId", item.getId());
        object.put("pageNumber", pageNumber);
        row.add(new InlineKeyboardButton().setText(item.getName()).setCallbackData("/ItemMenu/?" + object));
    }


    public Long getPageNumberFromCallback(String data) {
        JSONObject object = new JSONObject(data.substring(data.indexOf("{")));
        if (pageNumber == 0 || pageNumber < 0) {
            throw new RuntimeException("not find item by id=" + pageNumber);
        }
        return Long.valueOf(object.get("pageNumber").toString());
    }

    public Item getItemFromCallback(String data) {
        JSONObject object = new JSONObject(data.substring(data.indexOf("{")));
        Item item = itemService.findById(Long.valueOf(object.get("itemId").toString())).get(0);

        if (item == null) {
            throw new RuntimeException("not find item by id=" + 1);
        }

        return item;
    }
}
