package edu.telegram.springbot.container;

import org.junit.Before;
import org.junit.Test;
import org.telegram.telegrambots.api.methods.BotApiMethod;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BotApiMethodControllerTest {
    private BotApiMethodController controllerSingle;
    private BotApiMethodController controllerList;

    class TestBeanSingle{
        public BotApiMethod toDo(){
            return new SendMessage();
        }
    }

    class TestBeanList{
        public List<BotApiMethod> toDo(){
            return Arrays.asList(new SendMessage(), new SendMessage());
        }
    }


    @Before
    public void init() throws NoSuchMethodException {
        TestBeanList testBeanList = new TestBeanList();
        TestBeanSingle testBeanSingle = new TestBeanSingle();

        controllerList = new BotApiMethodController(testBeanList, testBeanList.getClass().getMethod("toDo", null)) {
            @Override
            public boolean successUpdatePredicate(Update update) {
                return true;
            }
        };

        controllerSingle = new BotApiMethodController(testBeanSingle, testBeanSingle.getClass().getMethod("toDo", null)) {
            @Override
            public boolean successUpdatePredicate(Update update) {
                return true;
            }
        };
    }


    @Test
    public void typeListDetect() throws Exception {
        assertFalse(controllerSingle.typeListReturnDetect());
        assertTrue(controllerList.typeListReturnDetect());
    }
}